#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>
#include<stddef.h>
#include"modul.h"

int main() {

    CircularLinkedList* myList;
    initializeList(&myList);

	int capacity, i, element, flag=0;
	char ImePrezime[100];
	int ocjena;
	
	printf("Unesite velicinu liste: ");
	scanf("%d", &capacity);

	//myList->maxCapacity=3;

//	srand((unsigned)time(NULL));

	for(i = 0; i < capacity; i++){
		flag=0;
		printf("Unesite ime i prezime studenta: ");
		scanf(" %[^\n]", ImePrezime);
		printf("Unesite ocjenu koju je student ostvario na ispitu: ");
		do{
			if(flag>0) printf("Pogresan unos ocjene, unesite ponovno: ");
			scanf("%d", &ocjena);
			flag++;
		}while(ocjena<1 || ocjena>5);
		addElement(myList, ImePrezime, ocjena);
	}

//	Node* oldTail=myList->tail->next;
//	Node* trenutni=oldTail->next;
//
//    printf("Sadrzaj liste: ");
//    while(trenutni != NULL && trenutni != oldTail) {
//    printf("%d ", trenutni->data);
//    trenutni = trenutni->next;
//    }
//    printf("\n");

	printList(myList);

	generiranjeIzvjestaja(myList);

	printf("\n\n\nBrisanje liste...\n\n");
	if (!isEmpty(myList)) {
	      deleteAllElement(myList);
	      printf("Lista je sada prazna.\n");
	  } else {
	      printf("Lista je vec prazna.\n");
	   }

	deleteList(myList);

    return 0;
}
