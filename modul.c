#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"modul.h"

void initializeList(CircularLinkedList** list) {
    *list = (CircularLinkedList*)malloc(sizeof(CircularLinkedList));
    if (*list == NULL) {
        printf("Greska pri alociranju memorije za listu");
    }

    (*list)->head = NULL;
    (*list)->tail = NULL;
    (*list)->capacity = 0;
}

void addElement(CircularLinkedList* list, char *ImePrezime, int ocjena) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    if (newNode == NULL) {
        printf("Greska pri alokaciji memorije za novi cvor");
    }
	
	strcpy(newNode->ImePrezime, ImePrezime);
//    newNode->ImePrezime = value;
	newNode->ocjena = ocjena;
	if(newNode->ocjena >= 2) newNode->prolaz = 1;
	else newNode->prolaz = 0;
    newNode->next = NULL;

    if (list->head == NULL) {
        list->head = newNode;
        list->tail = newNode;
        newNode->next = newNode;
    } else {
        list->tail->next = newNode;
        list->tail = newNode;
        newNode->next = list->head;
    }

    list->capacity++;
}

void deleteAllElement(CircularLinkedList* list) {
    Node* trenutni = list->head;
    Node* sljedeci;

	int i;

    for (i = 0; i < list->capacity; i++) {
        sljedeci = trenutni->next;
        free(trenutni);
        trenutni = sljedeci;
    }

    list->head = NULL;
    list->tail = NULL;
    list->capacity = 0;
}

void deleteList(CircularLinkedList* list){
	if(list == NULL){
		return;
	}

	deleteAllElement(list);
	free(list);
}

void printList(CircularLinkedList* list) {
    Node* trenutni = list->head;
	int i;

    printf("Sadrzaj liste: \n");
    for (i = 0; i < list->capacity; i++) {
    	printf("%d. Student\n", i+1);
    	printf("%s\n", trenutni->ImePrezime);
    	printf("Ocjena na ispitu: %d\n", trenutni->ocjena);
    	if(trenutni->prolaz) printf("Student je polozio ispit\n");
		else printf("Student nije polozio ispit\n"); 
      // 	printf("%d ", trenutni->data);
        trenutni = trenutni->next;
    }
    printf("\n\n");
}

int isEmpty(CircularLinkedList* list) {
    return list->head == NULL;
}

int isFull(CircularLinkedList* list) {

    return list->head != NULL && list->head->next ==list->tail;
}

void generiranjeIzvjestaja(CircularLinkedList* list) {
	int zbrojOcjena=0, brojPolozenih=0, brojPadova=0, ukupniBrojStudenata=0;
	int brojPetica=0, brojCetvorki=0, brojTrojki=0, brojDvojki=0;
	
	float prosjekOcjena;
	
	ukupniBrojStudenata=list->capacity;
	
    Node* trenutni = list->head;
	int i;
    
    for (i = 0; i < list->capacity; i++) {
    	zbrojOcjena+=trenutni->ocjena;
    	if(trenutni->ocjena >= 2) brojPolozenih++;
    	else brojPadova++;
    	
    	if(trenutni->ocjena == 5) brojPetica++;
    	if(trenutni->ocjena == 4) brojCetvorki++;
    	if(trenutni->ocjena == 3) brojTrojki++;
    	if(trenutni->ocjena == 2) brojDvojki++;
    	   
        trenutni = trenutni->next;
    }
   	
   	prosjekOcjena=(float)zbrojOcjena/(float)ukupniBrojStudenata;
   	
   	printf("--- IZVJESTAJ O ISPITIMA ---\n\n");
   	printf("Ispitu je pristupilo %d studenata\n", ukupniBrojStudenata);
   	printf("Ukupni prosjek ocjena na ispitu: %.2f\n", prosjekOcjena);
   	printf("Broj studenata koji su polozili ispit: %d\n", brojPolozenih);
   	printf("Broj studenata koji su pali na ispitu: %d\n", brojPadova);
   	printf("Broj studenata koji su polozili ispit s ocjenom 5: %d\n", brojPetica);
	printf("Broj studenata koji su polozili ispit s ocjenom 4: %d\n", brojCetvorki);
	printf("Broj studenata koji su polozili ispit s ocjenom 3: %d\n", brojTrojki);
	printf("Broj studenata koji su polozili ispit s ocjenom 2: %d\n", brojDvojki);   
	printf("\n----------------------------\n");      	
}
