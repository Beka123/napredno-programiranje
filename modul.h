#ifndef HEADER
#define HEADER
#include<stdio.h>

/*typedef struct Node{
	int data;
	struct Node* next;
}Node;*/

typedef struct Node{
	char ImePrezime[100];
	int ocjena;
	int prolaz;
	struct Node* next;
}Node;

typedef struct CircularLinkedList{
	Node* head;
    Node* tail;
	int capacity;
	//int maxCapacity;
}CircularLinkedList;

void initializeList(CircularLinkedList** list);

void addElement(CircularLinkedList* list, char *ImePrezime, int ocjena);

void deleteAllElement(CircularLinkedList* list);

void deleteList(CircularLinkedList* list);

void printList(CircularLinkedList* list);

int isEmpty(CircularLinkedList* list);

int isFull(CircularLinkedList* list);

void generiranjeIzvjestaja(CircularLinkedList* list);

#endif
